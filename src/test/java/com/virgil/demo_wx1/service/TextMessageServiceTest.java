package com.virgil.demo_wx1.service;

import com.virgil.demo_wx1.entity.WxTextMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TextMessageServiceTest {

    @Autowired
    private TextMessageService service;

    @Test
    public void takeInTextReplayTextMessage() throws Exception {

    }

    @Test
    public void findAllByUserNameTest() throws Exception {

        String userName = "天天";
        List<WxTextMessage> messages = service.findAllByUserName(userName);
        for (WxTextMessage message :
                messages) {
            System.out.println(message);
        }
    }

}