package com.virgil.demo_wx1.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.virgil.demo_wx1.entity.WxFileMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WxFileMessageServiceTest {

    @Autowired
    private WxFileMessageService service;

    @Test
    public void findAllByToUser() throws Exception {

        IPage<WxFileMessage> files = service.findAllByToUser("天天", 2);
        System.out.println(files.getCurrent());
        System.out.println(files.getTotal());
        System.out.println(files.getPages());
        for (WxFileMessage file :
                files.getRecords()) {

            System.out.println(file);
        }
    }

}