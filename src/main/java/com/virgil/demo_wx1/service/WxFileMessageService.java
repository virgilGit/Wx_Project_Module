package com.virgil.demo_wx1.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soecode.wxtools.bean.WxXmlMessage;
import com.soecode.wxtools.bean.WxXmlOutTextMessage;
import com.virgil.demo_wx1.entity.WxFileMessage;

import java.util.List;

public interface WxFileMessageService {

    /**
     * 用户访问指定文件，设置文件被浏览过
     * @param fileUrl
     */
    public void checkFileMessage(String fileUrl);

    /**
     * 接受文字消息"文件"，处理后，返回下载文件，组装成weChat信息
     * @param message
     * @return
     */
    public WxXmlOutTextMessage getFileMessage(WxXmlMessage message);

    /**
     * 接受文字消息"所有文件",处理后，返回连接，跳转到展示所有文件的界面
     */
    public WxXmlOutTextMessage getFilePage(WxXmlMessage message);

    /**
     * 通过被推送人，查询他所有被推送的文件列表
     * @param toUser
     * @return
     */
    public IPage<WxFileMessage> findAllByToUser(String toUser, Integer page);
}
