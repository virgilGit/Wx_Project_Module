package com.virgil.demo_wx1.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soecode.wxtools.bean.WxXmlMessage;
import com.soecode.wxtools.bean.WxXmlOutTextMessage;
import com.virgil.demo_wx1.entity.User;
import com.virgil.demo_wx1.entity.WxFileMessage;
import com.virgil.demo_wx1.helper.WxHelper;
import com.virgil.demo_wx1.mapper.WxFileMessageMapper;
import com.virgil.demo_wx1.service.WxFileMessageService;
import com.virgil.demo_wx1.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class WxFileMessageServiceImpl implements WxFileMessageService {

    @Autowired
    private WxFileMessageMapper wxFileMessageMapper;

    @Autowired
    private WxHelper wxHelper;

    @Override
    @Transactional
    public void checkFileMessage(String fileUrl) {

        WxFileMessage fileMessage = wxFileMessageMapper.findByFileUrl(fileUrl);
        fileMessage.setChecked(1);
        wxFileMessageMapper.updateById(fileMessage);
    }

    @Override
    @Transactional
    public WxXmlOutTextMessage getFileMessage(WxXmlMessage message) {

        User fromUser = wxHelper.getMessageInitiator(message);

        // 查询数据库，指定ToUserName
        StringBuilder stringBuilder = new StringBuilder();
        List<WxFileMessage> fileMessages = wxFileMessageMapper.findAllUnCheckedByToUser(fromUser.getName());

        if (fileMessages == null || fileMessages.isEmpty()) {

            // 优化： 这边可以加链接，让用户看所有的文档信息
            return wxHelper.assembleOutTextMessage(message.getFromUserName(), message.getToUserName(), "当前没有需要下载的文件哦！");
        }
        for (WxFileMessage fileMessage :
                fileMessages) {
            stringBuilder.append(fileMessage.getTitle() + "\n");
            String pushDateStr = DateUtil.format(fileMessage.getPushDate(), DateUtil.FORMAT_SHORT);
            stringBuilder.append(pushDateStr+"\n");
            stringBuilder.append("http://"+ wxHelper.getIp() +".ngrok.io/files/download?fileName="+fileMessage.getFileUrl() + "\n\n");
        }

        return wxHelper.assembleOutTextMessage(message.getFromUserName(), message.getToUserName(), stringBuilder.toString());
    }

    @Override
    public WxXmlOutTextMessage getFilePage(WxXmlMessage message) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("http://" + wxHelper.getIp() + ".ngrok.io/files/page?fromUserName="+message.getFromUserName());
        return wxHelper.assembleOutTextMessage(message.getFromUserName(), message.getToUserName(), stringBuilder.toString());
    }

    @Override
    public IPage<WxFileMessage> findAllByToUser(String toUser, Integer page) {

        Page<WxFileMessage> pageObj = new Page<>();
        pageObj.setCurrent(page);
        pageObj.setSize(5);
        return wxFileMessageMapper.findAllByToUser(pageObj, toUser);
    }
}
