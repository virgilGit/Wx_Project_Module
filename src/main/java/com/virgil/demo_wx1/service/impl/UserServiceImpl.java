package com.virgil.demo_wx1.service.impl;

import com.virgil.demo_wx1.entity.User;
import com.virgil.demo_wx1.mapper.UserMapper;
import com.virgil.demo_wx1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User findByWxUserName(String wxUserName) {

        return userMapper.findByWxUserName(wxUserName);
    }
}
