package com.virgil.demo_wx1.service.impl;

import com.soecode.wxtools.bean.WxXmlMessage;
import com.soecode.wxtools.bean.WxXmlOutNewsMessage;
import com.soecode.wxtools.bean.WxXmlOutTextMessage;
import com.virgil.demo_wx1.entity.User;
import com.virgil.demo_wx1.entity.WxTextMessage;
import com.virgil.demo_wx1.helper.WxHelper;
import com.virgil.demo_wx1.mapper.UserMapper;
import com.virgil.demo_wx1.mapper.WxFileMessageMapper;
import com.virgil.demo_wx1.mapper.WxTextMessageMapper;
import com.virgil.demo_wx1.service.TextMessageService;
import com.virgil.demo_wx1.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class TextMessageServiceImpl implements TextMessageService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private WxTextMessageMapper wxTextMessageMapper;

    @Autowired
    private WxFileMessageMapper wxFileMessageMapper;

    @Autowired
    private WxHelper wxHelper;

    @Override
    public WxXmlOutNewsMessage getStatementMessage(WxXmlMessage message) {

        // 这边以后传输chart message, 以后移植到ChartMessageService中
        String fromUserName = message.getFromUserName();
        WxXmlOutNewsMessage newsMessage = new WxXmlOutNewsMessage();
        WxXmlOutNewsMessage.Item item = new WxXmlOutNewsMessage.Item();
        item.setDescription("季度报表，请查收");
        item.setPicUrl("http://"+ wxHelper.getIp() +".ngrok.io/img/girl2.jpg");
        item.setTitle("季度报表");
        item.setUrl("http://"+ wxHelper.getIp() +".ngrok.io/charts/test");

        newsMessage.addArticle(item);
        newsMessage.setCreateTime(new Date().getTime());
        newsMessage.setFromUserName(message.getToUserName());
        newsMessage.setToUserName(message.getFromUserName());
        newsMessage.setMsgType("news");
        return newsMessage;
    }

    /**
     * 输入文本数据，根据判定，返回相应的文本数据；
     * 用户发送"消息" 返回，用户在数据库中所有的"消息"，
     * 消息格式：
     * xxxxxxxxxxxxxxxxx
     * xxxxxxxx
     *    2019.6.1 发送人
     * @param message
     * @return
     */
    @Transactional
    @Override
    public WxXmlOutTextMessage takeInTextReplayTextMessage(WxXmlMessage message) {

        // 请求发起方user
        String fromUserName = message.getFromUserName();
        User fromUser = wxHelper.getMessageInitiator(message);

        // 1. 查询数据库，查询所有ToUser是指定User的Message
        List<WxTextMessage> messages = wxTextMessageMapper.findAllByToUserName(fromUser.getName());

        // 2. 组装message，循环
        StringBuilder stringBuilder = new StringBuilder();
        for (WxTextMessage textMessage:
                messages) {
            stringBuilder.append(textMessage.getContent()+"\n");
            String pushDateStr = DateUtil.format(textMessage.getPushDate(), DateUtil.FORMAT_SHORT);
            stringBuilder.append(pushDateStr+"\n\n");
            // debug: 数据库查询推送信息
            System.out.println(textMessage);
        }
        stringBuilder.append("http://"+ wxHelper.getIp() +".ngrok.io/page?fromUserName="+fromUserName);

        // debug: 推送信息拼装格式
        System.out.println(stringBuilder.toString());

        // 3. 返回outMessage
        return wxHelper.assembleOutTextMessage(message.getFromUserName(),message.getToUserName(), stringBuilder.toString());
    }

    @Override
    public List<WxTextMessage> findAllByUserName(String userName) {

        return wxTextMessageMapper.findAllByToUserName(userName);
    }

}
