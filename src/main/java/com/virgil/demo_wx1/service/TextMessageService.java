package com.virgil.demo_wx1.service;

import com.soecode.wxtools.bean.WxXmlMessage;
import com.soecode.wxtools.bean.WxXmlOutNewsMessage;
import com.soecode.wxtools.bean.WxXmlOutTextMessage;
import com.virgil.demo_wx1.entity.WxTextMessage;

import java.util.List;

public interface TextMessageService {

    /**
     * 接受文字消息，处理后，返回图文报表（简化链接）
     * @param message
     * @return
     */
    WxXmlOutNewsMessage getStatementMessage(WxXmlMessage message);

    /**
     * 接受文字信息，处理后，返回相应的文字信息
     * @param message
     * @return
     */
    WxXmlOutTextMessage takeInTextReplayTextMessage(WxXmlMessage message);

    /**
     * 通过user的name字段，查询所有发送给该用户的所有推送
     * @param userName
     * @return list wxTextMessage
     */
    List<WxTextMessage> findAllByUserName(String userName);
}
