package com.virgil.demo_wx1.service;

import com.virgil.demo_wx1.entity.User;

public interface UserService {

    /**
     * 通过wxId 获取指定的User信息
     * @param wxUserName
     * @return user
     */
    User findByWxUserName(String wxUserName);
}
