package com.virgil.demo_wx1.helper;

import com.soecode.wxtools.bean.WxXmlMessage;
import com.soecode.wxtools.bean.WxXmlOutTextMessage;
import com.virgil.demo_wx1.entity.User;
import com.virgil.demo_wx1.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class WxHelper {

    private String ip = "b341a33e";

    @Autowired
    private UserMapper userMapper;

    public User getMessageInitiator(WxXmlMessage message) {

        // 请求发起方user
        String fromUserName = message.getFromUserName();
        User fromUser = userMapper.findByWxUserName(fromUserName);
        System.out.println(fromUser);

        if (fromUser == null) {

            fromUser = new User();
            fromUser.setName("匿名");
            fromUser.setWxUserName(fromUserName);
            userMapper.insert(fromUser);
        }
        return fromUser;
    }

    public WxXmlOutTextMessage assembleOutTextMessage(String toUserName,
                                                      String fromUserName,
                                                      String content) {

        WxXmlOutTextMessage wxXmlOutTextMessage = new WxXmlOutTextMessage();
        wxXmlOutTextMessage.setContent(content);
        wxXmlOutTextMessage.setCreateTime(new Date().getTime());
        wxXmlOutTextMessage.setMsgType("text");
        wxXmlOutTextMessage.setFromUserName(fromUserName);
        wxXmlOutTextMessage.setToUserName(toUserName);
        return wxXmlOutTextMessage;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
