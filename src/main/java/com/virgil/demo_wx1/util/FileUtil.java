package com.virgil.demo_wx1.util;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.UUID;

public class FileUtil {

    public static String getRealPath(String filePath, HttpServletRequest request) {
        String realPath = System.getProperty("user.dir")+"/src/main/resources/static/"+filePath;
        File file = new File(realPath);
        if(!file.exists()) {

            file.mkdir();
        }
        return realPath;
    }

    //保留源文件后缀，防止上传文件重名
    public static String getNewFileName(MultipartFile file) {
        //根据源文件对象，得到源文件后缀
        String originalFileName = file.getOriginalFilename();
        String name = originalFileName.substring(0,originalFileName.lastIndexOf("."));
        String suffix = originalFileName.substring(originalFileName.lastIndexOf("."));

        //产生随机文件名
        String uuid = UUID.randomUUID().toString();

        //最终文件名
        String finalName = name+uuid+suffix;
        return finalName;
    }
}
