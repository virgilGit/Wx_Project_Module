package com.virgil.demo_wx1;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.virgil.demo_wx1.mapper")
public class DemoWx1Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoWx1Application.class, args);
	}

}
