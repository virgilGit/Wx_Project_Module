package com.virgil.demo_wx1.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.virgil.demo_wx1.entity.WxFileMessage;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WxFileMessageMapper extends BaseMapper<WxFileMessage> {

    @Select("SELECT * FROM wx_file_message WHERE to_user = #{toUser}")
    public IPage<WxFileMessage> findAllByToUser(Page<WxFileMessage> page, @Param("toUser") String toUser);

    @Select("SELECT * FROM wx_file_message WHERE file_url = #{fileUrl}")
    public WxFileMessage findByFileUrl(@Param("fileUrl") String fileUrl);

    @Select("SELECT * FROM wx_file_message WHERE to_user = #{toUser} AND checked=0")
    public List<WxFileMessage> findAllUnCheckedByToUser(@Param("toUser") String toUser);
}
