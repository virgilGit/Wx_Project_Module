package com.virgil.demo_wx1.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.virgil.demo_wx1.entity.WxTextMessage;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("wxTextMessageMapper")
public interface WxTextMessageMapper extends BaseMapper<WxTextMessage> {

    /**
     * 查询指定用户所有被推送的信息
     * @param toUserName
     * @return
     */
    @Select("SELECT * FROM wx_text_message WHERE to_user = #{toUserName}")
    List<WxTextMessage> findAllByToUserName(@Param("toUserName") String toUserName);
}
