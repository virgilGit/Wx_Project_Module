package com.virgil.demo_wx1.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.virgil.demo_wx1.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository("userMapper")
public interface UserMapper extends BaseMapper<User> {

    @Select("SELECT * FROM user WHERE wx_user_name = #{wxUserName}")
    User findByWxUserName(@Param("wxUserName") String wxUserName);
}
