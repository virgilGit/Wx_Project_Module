package com.virgil.demo_wx1.web;

import com.soecode.wxtools.api.IService;
import com.soecode.wxtools.api.WxConsts;
import com.soecode.wxtools.api.WxMessageRouter;
import com.soecode.wxtools.api.WxService;
import com.soecode.wxtools.bean.WxXmlMessage;
import com.soecode.wxtools.bean.WxXmlOutMessage;
import com.soecode.wxtools.bean.WxXmlOutNewsMessage;
import com.soecode.wxtools.bean.WxXmlOutTextMessage;
import com.soecode.wxtools.util.xml.XStreamTransformer;
import com.virgil.demo_wx1.entity.User;
import com.virgil.demo_wx1.service.TextMessageService;
import com.virgil.demo_wx1.service.WxFileMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.event.MenuKeyEvent;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

@RestController
@RequestMapping("/wx")
public class WxController {

    @Autowired
    private TextMessageService textMessageService;

    @Autowired
    private WxFileMessageService wxFileMessageService;

    private IService iService = new WxService();

    @GetMapping
    public String check(String signature, String timestamp, String nonce, String echostr) {

        if (iService.checkSignature(signature,timestamp,nonce, echostr)) {
            return echostr;
        }
        return null;
    }

    @PostMapping
    public void handle(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException {

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        // 创建一个路由器
        WxMessageRouter router = new WxMessageRouter(iService);
        try {
            // 微信服务器推送过来的是XML格式。
            WxXmlMessage wx = XStreamTransformer.fromXml(WxXmlMessage.class, request.getInputStream());
            System.out.println("消息：\n " + wx.toString());

            if (wx.getContent().equals("消息")) {

                WxXmlOutTextMessage xmlOutTextMessage = textMessageService.takeInTextReplayTextMessage(wx);
                out.print(xmlOutTextMessage.toXml());
            } else if (wx.getContent().equals("报表")){

                WxXmlOutNewsMessage xmlOutNewsMessage = textMessageService.getStatementMessage(wx);
                out.print(xmlOutNewsMessage.toXml());
            } else if (wx.getContent().equals("文件")){

                WxXmlOutTextMessage xmlOutTextMessage = wxFileMessageService.getFileMessage(wx);
                out.print(xmlOutTextMessage.toXml());
            } else if (wx.getContent().equals("所有文件")) {

                WxXmlOutTextMessage xmlOutTextMessage = wxFileMessageService.getFilePage(wx);
                out.print(xmlOutTextMessage.toXml());
            } else {

                WxXmlOutTextMessage xmlOutTextMessage = new WxXmlOutTextMessage();
                xmlOutTextMessage.setToUserName(wx.getFromUserName());
                xmlOutTextMessage.setFromUserName(wx.getToUserName());
                xmlOutTextMessage.setMsgType("text");
                xmlOutTextMessage.setCreateTime(new Date().getTime());
                xmlOutTextMessage.setContent("无法识别的请求！");
                out.print(xmlOutTextMessage.toXml());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }
}
