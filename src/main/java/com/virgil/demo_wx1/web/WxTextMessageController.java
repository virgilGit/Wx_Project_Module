package com.virgil.demo_wx1.web;

import com.virgil.demo_wx1.entity.User;
import com.virgil.demo_wx1.entity.WxTextMessage;
import com.virgil.demo_wx1.service.TextMessageService;
import com.virgil.demo_wx1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class WxTextMessageController {

    @Autowired
    private UserService userService;

    @Autowired
    private TextMessageService textMessageService;

    @GetMapping("/page")
    public String turnToPage(String fromUserName, Model model) {

        User user = userService.findByWxUserName(fromUserName);
        System.out.println(user);
        model.addAttribute("client", user);
        return "welcome";
    }

    @GetMapping("/messages")
    @ResponseBody
    public List<WxTextMessage> findAllMessagesByUserName(String userName) {

        return textMessageService.findAllByUserName(userName);
    }

}
