package com.virgil.demo_wx1.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/charts")
public class WxChartMessageController {

    @GetMapping("/test")
    public String testChartPage(Model model) {

        return "chart_bar";
    }
}
