package com.virgil.demo_wx1.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.virgil.demo_wx1.entity.User;
import com.virgil.demo_wx1.entity.WxFileMessage;
import com.virgil.demo_wx1.mapper.UserMapper;
import com.virgil.demo_wx1.mapper.WxFileMessageMapper;
import com.virgil.demo_wx1.service.WxFileMessageService;
import com.virgil.demo_wx1.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;

@Controller
public class WxFileMessageController {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private WxFileMessageService wxFileMessageService;

    @GetMapping("/files/download")
    public String downLoadFIle(String fileName, HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {

        wxFileMessageService.checkFileMessage(fileName);

        String realPath = FileUtil.getRealPath(fileName, request);
        // debug: 查看下载文件路径
        System.out.println(realPath);
        File file = new File(realPath);

        if (file.exists()) {
            // 配置文件下载
            response.setHeader("content-type", "application/octet-stream");
            response.setContentType("application/octet-stream");
            // 下载文件能正常显示中文
            response.setHeader("Content-Disposition", "attachment;filename=" +
                    URLEncoder.encode(fileName, "UTF-8"));
            // 实现文件下载
            byte[] buffer = new byte[1024];
            FileInputStream fileInputStream = null;
            BufferedInputStream bufferedInputStream = null;
            try{
                fileInputStream = new FileInputStream(file);
                bufferedInputStream = new BufferedInputStream(fileInputStream);
                OutputStream outputStream = response.getOutputStream();
                int i = bufferedInputStream.read(buffer);
                while(i != -1) {
                    outputStream.write(buffer, 0, i);
                    i = bufferedInputStream.read(buffer);
                }
            }catch (Exception e) {
                e.printStackTrace();
            } finally {

                if (bufferedInputStream != null){
                    try {
                        bufferedInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }else {
            System.out.println("file does not exist");
        }
        return null;
    }

    @GetMapping("/files/page")
    public String fileListPage(String fromUserName, Model model) {

        User user = userMapper.findByWxUserName(fromUserName);
        model.addAttribute("client", user);
        return "file_list";
    }

    @GetMapping("/files/all")
    @ResponseBody
    public IPage<WxFileMessage> findAllByToUser(String userName, Integer page) {

        if (page == null || page == 0) {

        }
        return wxFileMessageService.findAllByToUser(userName, page);
    }
}
